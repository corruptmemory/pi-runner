# Pi-runner -- demoing properties of Go's runtime

A simple program that shows how Go's runtime deals with multiplexing N `goroutines` onto M real cores

## Compute-base load

```
./pi-runner cpu
```

Will compute digits of `pi`.  It will schedule a `goroutine` for each core on your machine.  It will
schedule the longest-running computations first.  The largest number of digits it will compute is
500K and then divide by 10 down to 5.

You can run this to monitor the number of OS threads started by the Go runtime:

```bash
$ watch 'ps huH p `pidof pi-runner` | wc -l'
```

Roughly the number of OS threads will be equal to the CPU count on your machine.  There may be 1 or
2 more threads used by the Go runtime for `goroutine` scheduling and garbage collection.

## IO-based load

```
./pi-runner cpu
```

This will read from `/dev/zero` (so won't work on Windows).  Again, it will schedule the largest
amount of data to read (50MB) per CPU core on your machine first.  Then repeatedly divide by 10 down
to reading 5 bytes from `/dev/zero`.

In this case you will see many more threads than CPU cores on your machine when running:

```bash
$ watch 'ps huH p `pidof pi-runner` | wc -l'
```

Go's runtime can distinguish between CPU-bound and IO bound workloads and decide to spawn more or
fewer OS threads accordingly.

This ability is Go's superpower.
