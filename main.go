package main

import (
	"fmt"
	"math/big"
	"os"
	"runtime"
	"sync"
	"time"
)

func pi(ndigits uint) string {
	digits := big.NewInt(int64(ndigits) + 10)
	unity := big.NewInt(0)
	unity.Exp(big.NewInt(10), digits, nil)
	pi := big.NewInt(0)
	four := big.NewInt(4)
	pi.Mul(four, pi.Sub(pi.Mul(four, arccot(5, unity)), arccot(239, unity)))
	return pi.String()[0:ndigits]
}

func arccot(x int64, unity *big.Int) *big.Int {
	bigx := big.NewInt(x)
	xsquared := big.NewInt(x * x)
	sum := big.NewInt(0)
	sum.Div(unity, bigx)
	xpower := big.NewInt(0)
	xpower.Set(sum)
	n := int64(3)
	zero := big.NewInt(0)
	sign := false

	term := big.NewInt(0)
	for {
		xpower.Div(xpower, xsquared)
		term.Div(xpower, big.NewInt(n))
		if term.Cmp(zero) == 0 {
			break
		}
		if sign {
			sum.Add(sum, term)
		} else {
			sum.Sub(sum, term)
		}
		sign = !sign
		n += 2
	}
	return sum
}

func runCompute(wg *sync.WaitGroup, id string, numDigits uint) {
	defer wg.Done()
	fmt.Printf("STARTED[%s]: pi-digits: %d\n", id, numDigits)
	_ = pi(numDigits)
	fmt.Printf("FINISHED[%s]: pi-digits: %d\n", id, numDigits)
}

func readZeros(numDigits uint) {
	f, err := os.Open("/dev/zero")
	if err != nil {
		fmt.Printf("Could not read /dev/zero: %v\n", err)
		return
	}
	defer f.Close()
	buf := [1]byte{}
	for i := uint(0); i < numDigits; i++ {
		f.Read(buf[:])
	}
}

func runIO(wg *sync.WaitGroup, id string, numDigits uint) {
	defer wg.Done()
	fmt.Printf("STARTED[%s]: IO: %d\n", id, numDigits)
	readZeros(numDigits)
	fmt.Printf("FINISHED[%s]: IO: %d\n", id, numDigits)
}

func computeLoad() {
	ncpu := runtime.NumCPU()
	fmt.Printf(">>> CPU count: %d\n", ncpu)
	wg := sync.WaitGroup{}

	start := time.Now()
	for c := 500000; c > 0; c = c / 10 {
		for i := 0; i < ncpu; i++ {
			wg.Add(1)
			go runCompute(&wg, fmt.Sprintf("%d-%d", c, i), uint(c))
		}
	}

	wg.Wait()
	end := time.Now()

	fmt.Printf(">>> Total time: %fs\n", end.Sub(start).Seconds())
}

func ioLoad() {
	ncpu := runtime.NumCPU()
	fmt.Printf(">>> CPU count: %d\n", ncpu)
	wg := sync.WaitGroup{}

	start := time.Now()
	for c := 50000000; c > 0; c = c / 10 {
		for i := 0; i < ncpu; i++ {
			wg.Add(1)
			go runIO(&wg, fmt.Sprintf("%d-%d", c, i), uint(c))
		}
	}

	wg.Wait()
	end := time.Now()

	fmt.Printf(">>> Total time: %fs\n", end.Sub(start).Seconds())
}

func main() {
	switch os.Args[1] {
	case "cpu":
		computeLoad()
	case "io":
		ioLoad()
	}
}
